<tag><tag>
<tag>
<tag>Gambardella, Matthew<tag>
<tag>XML Developer's Guide<tag>
<tag>Computer<tag>
<tag>44.95<tag>
<tag>2000-10-01<tag>
<tag>An in-depth look at creating applications
  with XML.<tag>
<tag>
<tag>
<tag>Ralls, Kim<tag>
<tag>Midnight Rain<tag>
<tag>Fantasy<tag>
<tag>5.95<tag>
<tag>2000-12-16<tag>
<tag>A former architect battles corporate zombies,
  an evil sorceress, and her own childhood to become queen
  of the world.<tag>
<tag>
<tag>
<tag>Corets, Eva<tag>
<tag>Maeve Ascendant<tag>
<tag>Fantasy<tag>
<tag>5.95<tag>
<tag>2000-11-17<tag>
<tag>After the collapse of a nanotechnology
  society in England, the young survivors lay the
  foundation for a new society.<tag>
<tag>
<tag>
<tag>Corets, Eva<tag>
<tag>Oberon's Legacy<tag>
<tag>Fantasy<tag>
<tag>5.95<tag>
<tag>2001-03-10<tag>
<tag>In post-apocalypse England, the mysterious
  agent known only as Oberon helps to create a new life
  for the inhabitants of London. Sequel to Maeve
  Ascendant.<tag>
<tag>
<tag>
<tag>Corets, Eva<tag>
<tag>The Sundered Grail<tag>
<tag>Fantasy<tag>
<tag>5.95<tag>
<tag>2001-09-10<tag>
<tag>The two daughters of Maeve, half-sisters,
  battle one another for control of England. Sequel to
  Oberon's Legacy.<tag>
<tag>
<tag>
<tag>Randall, Cynthia<tag>
<tag>Lover Birds<tag>
<tag>Romance<tag>
<tag>4.95<tag>
<tag>2000-09-02<tag>
<tag>When Carla meets Paul at an ornithology
  conference, tempers fly as feathers get ruffled.<tag>
<tag>
<tag>
<tag>Thurman, Paula<tag>
<tag>Splish Splash<tag>
<tag>Romance<tag>
<tag>4.95<tag>
<tag>2000-11-02<tag>
<tag>A deep sea diver finds true love twenty
  thousand leagues beneath the sea.<tag>
<tag>
<tag>
<tag>Knorr, Stefan<tag>
<tag>Creepy Crawlies<tag>
<tag>Horror<tag>
<tag>4.95<tag>
<tag>2000-12-06<tag>
<tag>An anthology of horror stories about roaches,
  centipedes, scorpions  and other insects.<tag>
<tag>
<tag>
<tag>Kress, Peter<tag>
<tag>Paradox Lost<tag>
<tag>Science Fiction<tag>
<tag>6.95<tag>
<tag>2000-11-02<tag>
<tag>After an inadvertant trip through a Heisenberg
  Uncertainty Device, James Salway discovers the problems
  of being quantum.<tag>
<tag>
<tag>
<tag>O'Brien, Tim<tag>
<tag>Microsoft .NET: The Programming Bible<tag>
<tag>Computer<tag>
<tag>36.95<tag>
<tag>2000-12-09<tag>
<tag>Microsoft's .NET initiative is explored in
  detail in this deep programmer's reference.<tag>
<tag>
<tag>
<tag>O'Brien, Tim<tag>
<tag>MSXML3: A Comprehensive Guide<tag>
<tag>Computer<tag>
<tag>36.95<tag>
<tag>2000-12-01<tag>
<tag>The Microsoft MSXML3 parser is covered in
  detail, with attention to XML DOM interfaces, XSLT processing,
  SAX and more.<tag>
<tag>
<tag>
<tag>Galos, Mike<tag>
<tag>Visual Studio 7: A Comprehensive Guide<tag>
<tag>Computer<tag>
<tag>49.95<tag>
<tag>2001-04-16<tag>
<tag>Microsoft Visual Studio 7 is explored in depth,
  looking at how Visual Basic, Visual C++, C#, and ASP+ are
  integrated into a comprehensive development
  environment.<tag>
<tag><tag>