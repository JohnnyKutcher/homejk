package hometest;

import interfaces.Fruit;
import org.junit.Test;

import java.util.Arrays;

/**
 * Created by aleksey on 17.06.17.
 */
public class SortFruitObject {

    @Test
    public void sortFruitObjects() {

        Fruit[] fruits = new Fruit[5];

        Fruit pineappale = new Fruit("Pineapple", "Pineapple description",70);
        Fruit apple = new Fruit("Apple", "Apple description",100);
        Fruit orange = new Fruit("Orange", "Orange description",80);
        Fruit banana = new Fruit("Banana", "Banana description",90);
        Fruit apple2 = new Fruit("Apple", "Apple description",110);

        fruits[0]=pineappale;
        fruits[1]=apple;
        fruits[2]=orange;
        fruits[3]=banana;
        fruits[4]=apple2;

//       Arrays.sort(fruits);

        Arrays.sort(fruits, Fruit.getFruitNameComparator());

        int i=0;
        for(Fruit temp: fruits){
            System.out.println("fruits " + ++i + " : " + temp.getFruitName() +
                    ", Quantity : " + temp.getQuantity());
        }

    }
}
























/*To sort an Object by its property, you have to make the Object implement the Comparable interface and override the compareTo() method. */