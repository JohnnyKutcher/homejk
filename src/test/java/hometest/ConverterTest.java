package hometest;

import org.junit.jupiter.api.Test;

import static xmlconv.XmlConverter.lineXml;
import static xmlconv.XmlConverter.newFileWOTags;
import static xmlconv.XmlConverter.replaceTags;

/**
 * Created by Admin on 14.06.2017.
 */
public class ConverterTest {
    @Test
    public void stringBldrFromXml() {
        String filename = "/home/johnny/IdeaProjects/apollo/src/main/resources/xml/hwXML.xml";
        System.out.println(lineXml(filename));
        newFileWOTags(lineXml(filename));
    }

    @Test
    public void replaceAllTags() {
        String filename = "/home/johnny/IdeaProjects/apollo/src/main/resources/xml/hwXML.xml";
        replaceTags(lineXml(filename));
    }
}