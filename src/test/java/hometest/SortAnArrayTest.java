package hometest;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by aleksey on 17.06.17.
 */
public class SortAnArrayTest {

    @Test
    public void sortAnArray() {

        String[] fruits = new String[]{"Pineapple", "Apple", "Orange", "Banana"};

        Arrays.sort(fruits);

        int i = 1;
        for (String temp : fruits) {
            System.out.println("fruits " + i++ + ": " + temp);
        }

    }

    @Test
    public void sortAnArrayList(){

        List<String> fruits = new ArrayList<>();

        fruits.add("Pineapple");
        fruits.add("Apple");
        fruits.add("Orange");
        fruits.add("Banana");

        Collections.sort(fruits);

        int i=0;
        for(String temp: fruits){
            System.out.println("fruits " + ++i + " : " + temp);
        }

    }
}
