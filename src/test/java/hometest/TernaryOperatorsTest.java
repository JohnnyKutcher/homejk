package hometest;

import operators.TernaryOperator;
import org.junit.jupiter.api.Test;

import static operators.TernaryOperator.*;

/**
 * Created by johnny on 26.06.17.
 */
public class TernaryOperatorsTest {
    @Test
    public void ternary() {
        String string = "Australia";
        int i = 10;
        TernaryOperator ternaryOperator = new TernaryOperator(5, 10);
        ternaryMethod(i, ternaryOperator);
        System.out.println(getMinValue(4,10));
        System.out.println(getAbsoluteValue(-10));
        System.out.println(invertBoolean(false));
        containsA(string);
    }
}
