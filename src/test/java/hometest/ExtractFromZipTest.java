package hometest;

import extract.from.zip.ExtractFromZip;
import org.junit.jupiter.api.Test;

/**
 * Created by johnny on 06.07.17.
 */
public class ExtractFromZipTest {
    @Test
    public void extractFromZip() {
        String zipFile = "/home/johnny/Downloads/horsehead-nebula-dark-nebula-constellation-orion-87646.jpeg.zip";
        String dir = "/home/johnny/Desktop/";
        ExtractFromZip efz = new ExtractFromZip();
        efz.unzip(zipFile, dir);
    }
}
