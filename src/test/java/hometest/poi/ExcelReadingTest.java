package hometest.poi;

import org.junit.Test;
import poi.CalculateFormula;
import poi.NewExcel;
import poi.NewHWExcel;
import poi.ReadSheet;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by johnny on 16.07.17.
 */
public class ExcelReadingTest {
    @Test
    public void test() {
        ReadSheet readSheet = new ReadSheet();
        readSheet.createNewXlsx("C:\\" +
                "Робота\\Робота\\Довідки\\Списання запчастин\\32 КП Лєтранс ВЧД Шепетівка.xlsx");
    }

    @Test
    public void test1() {
        NewExcel newExcel = new NewExcel("Writesheet.xlsx");

        Map<String, Object[]> empinfo = new TreeMap<>();
        empinfo.put("1", new Object[] {"EMP ID", "EMP NAME", "DESIGNATION"});
        empinfo.put("2", new Object[] {"tp01", "Gopal", "Technical Manager"});
        empinfo.put("3", new Object[] {"tp02", "Manisha", "Proof Reader"});
        empinfo.put("4", new Object[] {"tp03", "Masthan", "Technical Writer"});
        empinfo.put("5", new Object[] {"tp04", "Satish", "Technical Writer"});
        empinfo.put("6", new Object[] {"tp05", "Krishna", "Technical Writer"});

        newExcel.createXlxs(empinfo, "Employee Info");
    }

    @Test
    public void test2() {
        String[] strings = {"Principal","Interest","Time","Output (P * r * t)"};
        Integer[] integers = {1, 1000, 12, 6};

        CalculateFormula calculateFormula = new CalculateFormula("Writesheet2.xlsx");
        calculateFormula.calculateFormulaMethod("Calculate Simple Interest",
                0, strings, 1, integers, "A2*B2*C2");
    }

    @Test
    public void testHW() {

        String[] sheets = {"Ivan", "Anton", "Oleksander", "Aleksey", "Nikolai", "Igor", "Saribeg",
                "Nikita", "Taras", "Dmytriy"};
        String[] header = {"MONTH", "PAYCHECK"};
        Double[] salaryIvan = {400.00, 600.25, 528.36, 323.00, 421.20, 526.45, 623.80, 325.02,
                413.00, 215.30, 412.00, 400.00};
        Double[] salaryAnton = {333.00, 680.25, 512.36, 313.00, 421.20, 526.45, 623.80, 325.02,
                413.00, 215.30, 412.00, 400.00};
        Double[] salaryOleksander = {401.00, 690.25, 578.36, 223.00, 421.20, 526.45, 623.80, 325.02,
                413.00, 215.30, 412.00, 400.00};
        Double[] salaryAleksey = {4000.00, 6000.25, 5208.36, 3203.00, 4210.20, 5206.45, 6023.80, 3025.02,
                4103.00, 2105.30, 4102.00, 4000.00};
        Double[] salaryNikolai = {131.00, 600.25, 528.36, 393.00, 421.20, 526.45, 623.80, 325.02,
                413.00, 215.30, 412.00, 400.00};
        Double[] salaryIgor = {456.00, 600.25, 528.36, 323.00, 421.20, 526.45, 623.80, 325.02,
                413.00, 215.30, 4102.00, 400.00};
        Double[] salarySaribeg = {16.00, 600.25, 528.36, 323.00, 421.20, 526.45, 623.80, 325.02,
                413.00, 215.30, 412.00, 400.00};
        Double[] salaryNikita = {400.00, 600.25, 527.36, 323.00, 421.20, 526.45, 623.80, 325.02,
                413.00, 215.30, 412.00, 400.00};
        Double[] salaryTaras = {400.00, 600.25, 528.36, 323.00, 421.20, 526.45, 623.80, 325.02,
                413.00, 215.30, 412.00, 400.00};
        Double[] salaryDmytriy = {400.00, 400.25, 428.36, 423.00, 421.20, 526.45, 623.80, 325.02,
                413.00, 215.30, 412.00, 456.00};
        NewHWExcel newHWExcel = new NewHWExcel("Employee.xlsx", sheets);

        newHWExcel.createWorkbook(header, salaryIvan, salaryAnton, salaryOleksander, salaryAleksey, salaryNikolai,
                salaryIgor, salarySaribeg, salaryNikita, salaryTaras, salaryDmytriy);
    }
}
