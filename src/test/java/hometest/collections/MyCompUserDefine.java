package hometest.collections;

import collections.Empl;
import collections.MyNameComp;
import collections.MySalaryComp;
import org.junit.Test;

import java.util.TreeSet;

/**
 * Created by johnny on 11.07.17.
 */
public class MyCompUserDefine {
    @Test
    public void test() {
        TreeSet<Empl> nameComp = new TreeSet<>(new MyNameComp());
        nameComp.add(new Empl("Ram", 1000));
        nameComp.add(new Empl("Sam", 5000));
        nameComp.add(new Empl("John", 3000));
        nameComp.add(new Empl("Tom", 7500));

        for (Empl e: nameComp) {
            System.out.println(e);
        }
        System.out.println("=========================");
        TreeSet<Empl> salaryComp = new TreeSet<>(new MySalaryComp());
        salaryComp.add(new Empl("Ram", 1000));
        salaryComp.add(new Empl("Sam", 5000));
        salaryComp.add(new Empl("John", 3000));
        salaryComp.add(new Empl("Tom", 7500));
        for (Empl e: salaryComp) {
            System.out.println(e);
        }
    }
}
