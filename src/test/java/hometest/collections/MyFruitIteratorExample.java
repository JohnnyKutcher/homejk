package hometest.collections;

import collections.FruitIterator;
import collections.MyOwnArrayList;
import org.junit.Test;

/**
 * Created by johnny on 11.07.17.
 */
public class MyFruitIteratorExample {
    @Test
    public void test() {
        MyOwnArrayList fruitList = new MyOwnArrayList();
        fruitList.add("Mango");
        fruitList.add("Strawberry");
        fruitList.add("Papaya");
        fruitList.add("Apple");

        System.out.println("-----Calling my iterator on my ArrayList------");

        FruitIterator it = fruitList.iterator();
        while (it.hasNext()) {
            String s = (String) it.next();
            System.out.println("Value: " + s);
        }
        System.out.println("---Fruit list size: " + fruitList.size());
        fruitList.remove(1);
        System.out.println("---After removal, Fruit Size: " +  fruitList.size());

        for (int i = 0; i < fruitList.size(); i++) {
            System.out.println(fruitList.get(i));
        }
    }
}
