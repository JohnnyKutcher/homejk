package hometest.linkedlist;

import linkedlist.ListIterator;
import linkedlist.MyOwnLinkedList;
import org.junit.Test;

/**
 * Created by johnny on 13.07.17.
 */
public class MyLinkedListTest {
    @Test
    public void test() {
        MyOwnLinkedList linkedList = new MyOwnLinkedList();
        linkedList.add(7);
        linkedList.add(4);
        linkedList.add(2);
        linkedList.add(3);
        linkedList.add(8);
        linkedList.add(9);
        linkedList.add(6);

//        ListIterator listIterator = linkedList.iterator();
//        while (listIterator.hasNext()) {
//            System.out.println(listIterator.next());
//        }
//
//        System.out.println();
//
//        while (listIterator.hasPrevious()) {
//            System.out.println(listIterator.previous());
//        }
//
//        System.out.println();
//
//        while (listIterator.hasNext()) {
//            System.out.println(listIterator.next());
//        }

        System.out.println(linkedList.size());
        System.out.println();
        linkedList.add(0,9);

        for (int i = 0; i < linkedList.size(); i++) {
            System.out.println(linkedList.get(i));
        }
    }
}
