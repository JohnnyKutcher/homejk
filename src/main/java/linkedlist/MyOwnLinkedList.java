package linkedlist;

import java.io.Serializable;
import java.util.Arrays;
import java.util.RandomAccess;

/**
 * Created by Admin on 12.07.2017.
 */
public class MyOwnLinkedList implements RandomAccess, Cloneable, Serializable {

    /**
     *
     */
    private transient Object[] elementData;

    /**
     *
     */
    private int size;

    /**
     *
     */
    protected transient int modCount = 0;

    /**
     *
     */
    private static final long serialVersionUID = 1234L;

    public MyOwnLinkedList() {
        this(10);
    }

    public MyOwnLinkedList(int initialCapacity) {
        super();
        if (initialCapacity < 0) {
            throw new IllegalArgumentException("Illegal Capacity: " + initialCapacity);
        }
        this.elementData = new Object[initialCapacity];
    }

    /**
     *
     * @param obj
     * @return
     */
    public boolean add(Object obj) {
        validateCapacity(size + 1);
        elementData[size++] = obj;
        return true;
    }

    /**
     *
     * @param position
     * @param obj
     * @return
     */
    public boolean add(int position, Object obj) {
        Object[] newData = new Object[size + 1];
        if (position > elementData.length && position < 0) {
            throw new IndexOutOfBoundsException("Index: " + position + ", Size: " + elementData.length);
        } else {
            System.arraycopy(elementData, 0, newData, 0, position);
            System.arraycopy(elementData, position, newData, position + 1, size - position);
            newData[position] = obj;
            validateCapacity(size + 1);

            elementData = newData;
        }
        return true;
    }

    /**
     *
     * @param position
     * @param obj
     * @return
     */
    public boolean set(int position, Object obj) {
        if (position > elementData.length && position < 0) {
            throw new IndexOutOfBoundsException("Index: " + position + ", Size: " + elementData.length);
        } else {
            elementData[position] = obj;
        }
        return true;
    }

    /**
     *
     * @param index
     * @return
     */
    public Object get(int index) {
        Object obj = elementData[index];
        return obj;
    }

    /**
     *
     * @return
     */
    public int size() {
        return size;
    }

    /**
     *
     * @param minCapacity
     */
    public void validateCapacity(int minCapacity) {
        modCount++;
        int oldCapacity = elementData.length;
        if (minCapacity > oldCapacity) {
            Object[] oldData = elementData;
            int newCapacity = (oldCapacity * 3) / 2 + 1;

            if (newCapacity > minCapacity) {
                newCapacity = minCapacity;
            }

            elementData = new Object[newCapacity];
            System.arraycopy(oldData, 0, elementData, 0, size);
        }
    }

    /**
     *
     * @param index
     * @return
     */
    public Object remove(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        }
        modCount++;
        Object oldValue = elementData[index];

        int numMoved = size - index - 1;
        if (numMoved > 0) {
            System.arraycopy(elementData, index + 1, elementData,  index, numMoved);
        }
        elementData[--size] = null;
        return oldValue;
    }

    /**
     *
     * @param start
     * @param end
     * @return
     */
    public Object subList(int start, int end) {
        if (start >= size) {
            throw new IndexOutOfBoundsException("Start: " + start + ", Size: " + size);
        } else if (end >= size) {
            throw new IndexOutOfBoundsException("End: " + end + ", Size: " + size);
        }
        modCount++;
        int size = end - start;
        Object[] subL = new Object[size];
        System.arraycopy(elementData, start, subL, 0, size);
        return Arrays.toString(subL);
    }

    /**
     *
     * @param obj
     * @return
     */
    public int indexOf(Object obj) {
        int iOf = 0;
        for (int i = 0; i < elementData.length; i++) {
            if (elementData[i] == obj) {
                iOf = i;
                break;
            } else {
                iOf = -1;
            }
        }
        return iOf;
    }

    /**
     *
     * @param obj
     * @return
     */
    public int lastIndexOf(Object obj) {
        int iOf = 0;
        for (int i = elementData.length - 1; i >= 0; i--) {
            if (elementData[i] == obj) {
                iOf = i;
                break;
            } else {
                iOf = -1;
            }
        }
        return iOf;
    }

    /**
     *
     * @param obj
     */
    public void addLast(Object obj) {
        validateCapacity(size + 1);
        elementData[size++] = obj;
    }


    /**
     *
     * @param obj
     */
    public void addFirst(Object obj) {
        validateCapacity(size + 1);
        modCount++;
        System.arraycopy(elementData, 0, elementData, 1, size++);
        elementData[0] = obj;
    }

    /**
     *
     * @return
     */
    public Object getLast() {
        Object obj = elementData[size - 1];
        return obj;
    }

    /**
     *
     * @return
     */
    public Object getFirst() {
        Object obj = elementData[0];
        return obj;
    }

    /**
     *
     * @return
     */
    public ListIterator iterator() {
        return new ListIterator(this);
    }
}
