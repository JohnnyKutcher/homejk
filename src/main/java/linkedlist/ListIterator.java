package linkedlist;


/**
 * Created by Admin on 12.07.2017.
 */
public class ListIterator {

    /**
     *
     */
    private MyOwnLinkedList linkedList;

    /**
     *
     */
    private int position;

    public ListIterator(MyOwnLinkedList linkedList) {
        this.linkedList = linkedList;
    }

    /**
     *
     * @return
     */
    public boolean hasNext() {
        if (position < linkedList.size()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @return
     */
    public boolean hasPrevious() {
        if (position <= linkedList.size() && position >= 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @return
     */
    public int nextIndex() {
        int nextInd = position;
        return nextInd;
    }

    /**
     *
     * @return
     */
    public int previousIndex() {
        int nextInd = position;
        if (position == -1) {
            return position;
        } else {
            return nextInd;
        }
    }

    /**
     *
     * @return
     */
    public Object next() {
        Object anyObj = linkedList.get(position);
        position++;
        return anyObj;
    }

    /**
     *
     * @return
     */
    public Object previous() {

        position--;

        if (position >= 0) {
            return linkedList.get(position);
        } else {
            return -1;
        }


    }

    /**
     *
     */
    public void remove() {
        linkedList.remove(position);
    }

    /**
     *
     * @param obj
     */
    public void set(Object obj) {
        linkedList.set(position, obj);
    }
}
