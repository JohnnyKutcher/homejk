package poi;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

/**
 * Created by johnny on 16.07.17.
 */
public class ReadSheet {

    /**
     *
     */
    private int quantityOfSheets;

    public ReadSheet(int quantityOfSheets) {
        this.quantityOfSheets = quantityOfSheets;
    }

    public ReadSheet() {
        this.quantityOfSheets = 0;

    }

    /**
     *
     * @param pathName
     */
    public void createNewXlsx(String pathName) {
        try (FileInputStream fis = new FileInputStream(new File(pathName))) {
            XSSFWorkbook workbook = new XSSFWorkbook(fis);
            XSSFSheet spreadsheet = workbook.getSheetAt(quantityOfSheets);
            XSSFRow row;
            Iterator<Row> rowIterator = spreadsheet.iterator();
            while (rowIterator.hasNext()) {
                row = (XSSFRow) rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    switch (cell.getCellType()) {
                        case Cell.CELL_TYPE_NUMERIC:
                            System.out.print(cell.getNumericCellValue() + " \t\t ");
                            break;
                        case Cell.CELL_TYPE_STRING:
                            System.out.print(cell.getStringCellValue() + " \t\t ");
                            break;
                    }
                }
                System.out.println();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getQuantityOfSheets() {
        return quantityOfSheets;
    }

    public void setQuantityOfSheets(int quantityOfSheets) {
        this.quantityOfSheets = quantityOfSheets;
    }
}
