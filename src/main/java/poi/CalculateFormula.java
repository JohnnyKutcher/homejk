package poi;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by johnny on 16.07.17.
 */
public class CalculateFormula {

    /**
     *
     */
    private String pathName;

    public String getPathName() {
        return pathName;
    }

    public void setPathName(String pathName) {
        this.pathName = pathName;
    }

    public CalculateFormula(String pathName) {
        this.pathName = pathName;
    }

    /**
     *
     * @param headerRowNum
     * @param sheet
     * @param strings
     */
    public static void createHeader(int headerRowNum, XSSFSheet sheet, String[] strings) {
        Row header = sheet.createRow(headerRowNum);
        for (int i = 0; i < strings.length; i++) {
            header.createCell(i).setCellValue(strings[i]);
        }
    }

    /**
     *
     * @param datarRowNum
     * @param sheet
     * @param integersData
     * @param strings
     */
    public static void createDataRow(int datarRowNum, XSSFSheet sheet, Integer[] integersData, String... strings) {
        Row header = sheet.createRow(datarRowNum);
        for (int i = 0; i < integersData.length; i++) {
            header.createCell(i).setCellValue(integersData[i]);
        }
        for (int i = integersData.length + 1; i < strings.length; i++) {
            for (int j = 0; j < strings.length; j++) {
                header.createCell(i).setCellFormula(strings[i]);
            }
        }
    }

    /**
     *
     * @param sheetName
     * @param headerRow
     * @param stringsH
     * @param dataRow
     * @param integersData
     * @param strings
     */
    public void calculateFormulaMethod(String sheetName, int headerRow, String[] stringsH,
                                       int dataRow, Integer[] integersData, String... strings) {
        XSSFWorkbook workbook = new XSSFWorkbook();

        XSSFSheet sheet = workbook.createSheet(sheetName);

        CalculateFormula.createHeader(headerRow, sheet, stringsH);

        CalculateFormula.createDataRow(dataRow, sheet, integersData, strings);

        try (FileOutputStream out = new FileOutputStream(new File(pathName))) {
            workbook.write(out);
            out.flush();
            System.out.println("Excel File with formula is created!");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}




