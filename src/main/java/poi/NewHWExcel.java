package poi;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created by johnny on 19.07.17.
 */
public class NewHWExcel {
    /**
     *
     */
    private String pathName;

    /**
     * this is assignment of the number of sheets
     */
    private Integer sheetsNumber;

    public String[] getSheetsName() {
        return sheetsName;
    }

    public void setSheetsName(String[] sheetsName) {
        this.sheetsName = sheetsName;
    }

    /**
     *
     */
    private String[] sheetsName;

    public NewHWExcel(String pathName, String[] sheetsName) {
        this.pathName = pathName;
        this.sheetsNumber = sheetsName.length;
        this.sheetsName = sheetsName;
    }

    /**
     *
     * @param headerRowNum
     * @param sheet
     * @param strings
     */
    public static void createHeader(int headerRowNum, XSSFSheet sheet, String[] strings) {
        Row header = sheet.createRow(headerRowNum);
        for (int i = 0; i < strings.length; i++) {
            header.createCell(i).setCellValue(strings[i]);
        }
    }


    /**
     *
     * @param header
     * @param salary
     */
    public void createWorkbook(String[] header, Double[]... salary) {

        XSSFWorkbook workbook = new XSSFWorkbook();
        for (int i = 0; i < sheetsNumber; i++) {
            NewHWExcel.createSheet(workbook, header, sheetsName[i], salary[i]);

        }
        try (FileOutputStream out = new FileOutputStream(new File(pathName))) {
            workbook.write(out);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(pathName + " written successfully");
    }

    /**
     *
     * @param emplSalary
     * @return
     */
    public static Map<Integer, Object[]> createPaycheck(Double[] emplSalary) {

        String[] year = {"January", "February", "March", "April", "May", "June", "July", "August", "September",
                "October", "November", "December"};


        Map<Integer, Object[]> empinfo = new TreeMap<>();

        for (int i = 0, j = 1;  i < emplSalary.length; i++, j++) {
            empinfo.put(j, new Object[] {year[i], emplSalary[i]});
        }

        return empinfo;

    }

    /**
     *
     * @param workbook
     * @param header
     * @param salary
     * @param sheetName
     */
    public static void createSheet(XSSFWorkbook workbook, String[] header, String sheetName, Double[]... salary) {

        XSSFSheet spreadsheet = workbook.createSheet(sheetName);
        NewHWExcel.createHeader(0, spreadsheet, header);

        for (int i = 0; i < salary.length; i++) {
            XSSFRow row;

            NewHWExcel.createPaycheck(salary[i]);

            Set<Integer> keyId = NewHWExcel.createPaycheck(salary[i]).keySet();
            int rowid = 1;
            for (Integer key: keyId) {
                row = spreadsheet.createRow(rowid++);
                Object[] object = NewHWExcel.createPaycheck(salary[i]).get(key);
                int cellid = 0;
                for (Object obj : object) {
                    Cell cell = row.createCell(cellid++);
                    if (obj.getClass().getName().toString().equals("java.lang.String")) {
                        cell.setCellValue((String) obj);
                    } else if (obj.getClass().getName().toString().equals("java.lang.Double")) {
                        cell.setCellValue((Double) obj);
                    }
                }
            }
        }

    }


    public String getPathName() {
        return pathName;
    }

    public void setPathName(String pathName) {
        this.pathName = pathName;
    }

    public Integer getSheetsNumber() {
        return sheetsNumber;
    }

    public void setSheetsNumber(Integer sheetsNumber) {
        this.sheetsNumber = sheetsNumber;
    }
}
