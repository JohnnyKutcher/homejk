package poi;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * Created by johnny on 16.07.17.
 */
public class NewExcel {
    public String getPathName() {
        return pathName;
    }

    public void setPathName(String pathName) {
        this.pathName = pathName;
    }

    /**
     *
     */
    private String pathName;

    public NewExcel(String pathName) {
        this.pathName = pathName;
    }

    /**
     *
     * @param empinfo
     * @param sheetName
     */
    public void createXlxs(Map<String, Object[]> empinfo, String sheetName) {
        XSSFWorkbook workbook = new XSSFWorkbook();

        XSSFSheet spreadsheet = workbook.createSheet(sheetName);

        XSSFRow row;

        Set<String> keyId = empinfo.keySet();
        int rowid = 0;
        for (String key: keyId) {
            row = spreadsheet.createRow(rowid++);
            Object[] object = empinfo.get(key);
            int cellid = 0;
            for (Object obj : object) {
                Cell cell = row.createCell(cellid++);
                cell.setCellValue((String) obj);
            }
        }
        try (FileOutputStream out = new FileOutputStream(new File(pathName))) {
            workbook.write(out);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(pathName + " written successfully");
    }
}
