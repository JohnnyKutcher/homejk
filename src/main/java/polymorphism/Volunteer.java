package polymorphism;

/**
 * Created by johnny on 21.06.17.
 */
public class Volunteer extends StaffMember {
    /**
     *
     * @param eName
     * @param eAddress
     * @param ePhone
     */
    public Volunteer(String eName, String eAddress, String ePhone) {
        super(eName, eAddress, ePhone);
    }

    /**
     *
     * @return
     */
    @Override
    public double pay() {
        return 0.0;
    }


}
