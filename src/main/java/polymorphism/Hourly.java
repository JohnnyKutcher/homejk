package polymorphism;

/**
 * Created by johnny on 21.06.17.
 */
public class Hourly extends Employee {
    /**
     *
     */
    private int hoursWorked;

    /**
     *
     * @param eName
     * @param eAddress
     * @param ePhone
     * @param socSecurityNumber
     * @param rate
     */
    public Hourly(String eName, String eAddress, String ePhone,
                  String socSecurityNumber, double rate) {
        super(eName, eAddress, ePhone, socSecurityNumber, rate);
        hoursWorked = 0;
    }

    /**
     *
     * @param moreHours
     */
    public void addHours(int moreHours) {
        hoursWorked += moreHours;
    }

    /**
     *
     * @return
     */
    @Override
    public double pay() {
        double payment = payRate * hoursWorked;
        hoursWorked = 0;
        return payment;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        String result = super.toString();
        result += "\nCurrent hours: " + hoursWorked;
        return result;
    }
}
