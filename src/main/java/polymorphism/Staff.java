package polymorphism;

import java.util.ArrayList;

/**
 * Created by johnny on 21.06.17.
 */
public class Staff {
    /**
     *
     */
    private ArrayList<StaffMember> staffList;

    /**
     *
      * @param staffList
     */
    public Staff(ArrayList<StaffMember> staffList) {
        this.staffList = staffList;
    }

    /**
     *
     */
    public void payday() {
        double amount;
        for (int count = 0; count < staffList.size(); count++) {
            System.out.println(staffList.get(count));
            amount = staffList.get(count).pay();
            if (amount == 0.0) {
                System.out.println("Thanks!");
            } else {
                System.out.println("Paid: " + amount);
                System.out.println();
            }
        }
    }
}
