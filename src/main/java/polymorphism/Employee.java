package polymorphism;

/**
 * Created by johnny on 21.06.17.
 */
public class Employee extends StaffMember {
    /**
     *
     */
    protected String socialSecurityNumber;
    /**
     *
     */
    protected double payRate;

    /**
     *
     * @param eName
     * @param eAddress
     * @param ePhone
     * @param socSecurityNumber
     * @param rate
     */
    public Employee(String eName, String eAddress, String ePhone, String socSecurityNumber, double rate) {
        super(eName, eAddress, ePhone);
        this.socialSecurityNumber = socSecurityNumber;
        this.payRate = rate;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        String result = super.toString();
        result  += "\nSocial Security Number: " + socialSecurityNumber;
        return result;
    }

    /**
     *
     * @return
     */
    @Override
    public double pay() {
        return payRate;
    }
}
