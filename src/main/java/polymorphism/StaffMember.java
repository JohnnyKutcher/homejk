package polymorphism;

/**
 * Created by johnny on 21.06.17.
 */
public abstract class StaffMember {
    /**
     *
     */
    private String name;
    /**
     *
     */
    private String address;
    /**
     *
     */
    private String phone;

    /**
     *
     * @param eName
     * @param eAddress
     * @param ePhone
     */
    protected StaffMember(String eName, String eAddress, String ePhone) {
        this.name = eName;
        this.address = eAddress;
        this.phone = ePhone;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        String result = "Name: " + name + "\n";
        result += "Address: " + address + "\n";
        result += "Phone: " + phone + "\n";
        return result;
    }

    /**
     *
     * @return
     */
    public abstract double pay();
}
