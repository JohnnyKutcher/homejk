package collections;

import java.io.Serializable;
import java.util.RandomAccess;

/**
 * Created by johnny on 11.07.17.
 */
public class MyOwnArrayList implements RandomAccess, Cloneable, Serializable {
    /**
     *
     */
    private transient Object[] elementData;
    /**
     *
     */
    private int size;
    /**
     *
     */
    protected transient int modCount = 0;
    /**
     *
     */
    private static final long serialVersionUID = 1234L;

    public MyOwnArrayList() {
        this(10);
    }

    public MyOwnArrayList(int initialCapacity) {
        super();
        if (initialCapacity < 0) {
            throw new IllegalArgumentException("Illegal Capacity: " + initialCapacity);
        }
        this.elementData = new Object[initialCapacity];
    }

    /**
     *
     * @param obj
     * @return
     */
    public boolean add(Object obj) {
        validateCapacity(size + 1);
        elementData[size++] = obj;
        return true;
    }

    /**
     *
     * @param index
     * @return
     */
    public Object get(int index) {
        Object obj = elementData[index];
        return obj;
    }

    /**
     *
     * @return
     */
    public int size() {
        return size;
    }

    /**
     *
     * @param minCapacity
     */
    public void validateCapacity(int minCapacity) {
        modCount++;
        int oldCapacity = elementData.length;
        if (minCapacity > oldCapacity) {
            Object[] oldData = elementData;
            int newCapacity = (oldCapacity * 3) / 2 + 1;

            if (newCapacity > minCapacity) {
                newCapacity = minCapacity;
            }

            elementData = new Object[newCapacity];
            System.arraycopy(oldData, 0, elementData, 0, size);
        }
    }

    /**
     *
     * @param index
     * @return
     */
    public Object remove(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        }
            modCount++;
            Object oldValue = elementData[index];

            int numMoved = size - index - 1;
            if (numMoved > 0) {
                System.arraycopy(elementData, index + 1, elementData,  index, numMoved);
            }
            elementData[--size] = null;
            return oldValue;
    }

    /**
     *
     * @return
     */
    public FruitIterator iterator() {
        System.out.println("My overridded iterator method called in fruit class");
        return new FruitIterator(this);
    }
}
