package collections;

/**
 * Created by johnny on 11.07.17.
 */
public class Empl {
    /**
     *
     */
    private String name;
    /**
     *
     */
    private int salary;

    public Empl(String n, int s) {
        this.name = n;
        this.salary = s;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public int getSalary() {
        return salary;
    }

    /**
     *
     * @param salary
     */
    public void setSalary(int salary) {
        this.salary = salary;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Name " + this.name + "-- Salary: " + this.salary;
    }
}
