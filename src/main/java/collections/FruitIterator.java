package collections;

/**
 * Created by johnny on 11.07.17.
 */
public class FruitIterator {
    /**
     *
     */
    private MyOwnArrayList fruitList;
    /**
     *
     */
    private int position;

    public FruitIterator(MyOwnArrayList fruitList) {
        this.fruitList = fruitList;
    }

    /**
     *
     * @return
     */
    public boolean hasNext() {
        if (position < fruitList.size()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @return
     */
    public Object next() {
        Object anyObj = fruitList.get(position);
        position++;
        return anyObj;
    }

    /**
     *
     */
    public void remove() {
        fruitList.remove(position);
    }
}
