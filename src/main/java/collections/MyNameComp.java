package collections;

import java.util.Comparator;

/**
 * Created by johnny on 11.07.17.
 */
public class MyNameComp implements Comparator<Empl> {
    /**
     *
     * @param oOne
     * @param oTwo
     * @return
     */
    @Override
    public int compare(Empl oOne, Empl oTwo) {
        return oOne.getName().compareTo(oTwo.getName());
    }
}
