package regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by johnny on 18.06.17.
 */
public class Regex {
    protected Regex() {
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args) {

        Pattern pt = Pattern.compile("([a-zA-Z0-9\\s&&[^fF]])*([\\s])*");
        Matcher m = pt.matcher("165 163g       ");
        boolean ft = m.matches();
        System.out.println("ft = " + ft);
    }
}
