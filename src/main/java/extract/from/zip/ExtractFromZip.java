package extract.from.zip;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by johnny on 06.07.17.
 */
public class ExtractFromZip {
    /**
     *
     * @param zipPath
     * @param outputPath
     */
    public void unzip(String zipPath, String outputPath) {
        File direct = new File(outputPath);
        if (!direct.exists()) {
            direct.mkdir();
        }
        try (ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(zipPath))) {
            ZipEntry entry = zipInputStream.getNextEntry();
            while (entry != null) {
                String filePath = outputPath + File.separator + entry.getName();
                if (!entry.isDirectory()) {
                    extractFile(zipInputStream, filePath);
                } else {
                    File path = new File(filePath);
                    path.mkdir();
                }
                zipInputStream.closeEntry();
                entry = zipInputStream.getNextEntry();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param zipInputStream
     * @param filePath
     */
    private void extractFile(ZipInputStream zipInputStream, String filePath) {
        try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath))) {
            byte[] bytes = new byte[1024];
            int read = 0;
            while ((read = zipInputStream.read(bytes)) != -1) {
                bos.write(bytes, 0, read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
