package interfaces;

import java.util.Comparator;

/**
 * Created by aleksey on 17.06.17.
 */
public class Fruit implements Comparable<Fruit> {

    /**
     *
     */
    private String fruitName;
    /**
     *
     */
    private String fruitDesc;
    /**
     *
     */
    private int quantity;

    public Fruit(String fruitName, String fruitDesc, int quantity) {
        super();
        this.fruitName = fruitName;
        this.fruitDesc = fruitDesc;
        this.quantity = quantity;
    }

    public String getFruitName() {
        return fruitName;
    }

    public void setFruitName(String fruitName) {
        this.fruitName = fruitName;
    }

    public String getFruitDesc() {
        return fruitDesc;
    }

    public void setFruitDesc(String fruitDesc) {
        this.fruitDesc = fruitDesc;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public int compareTo(Fruit compareFruit) {

        int compareQuantity = compareFruit.getQuantity();

        //ascending order
        return this.quantity - compareQuantity;

        //descending order
        //return compareQuantity - this.quantity;
    }


    public static Comparator<Fruit> getFruitNameComparator() {
        return fruitNameComparator;
    }

    public static void setFruitNameComparator(Comparator<Fruit> fruitNameComparator) {
        Fruit.fruitNameComparator = fruitNameComparator;
    }

    /**
     *
     */
    private static Comparator<Fruit> fruitNameComparator = new Comparator<Fruit>() {

        @Override
        public int compare(Fruit fruitOne, Fruit fruitTwo) {

            String fruitNameOne = fruitOne.getFruitName().toUpperCase();
            String fruitNameTwo = fruitTwo.getFruitName().toUpperCase();

            //ascending order
            //return fruitNameOne.compareTo(fruitNameTwo);

            //descending order
            return fruitNameTwo.compareTo(fruitNameOne);
        }

    };
}

   /* The Comparable interface is only allow to sort a single property. To sort with multiple properties,
   you need Comparator.*/