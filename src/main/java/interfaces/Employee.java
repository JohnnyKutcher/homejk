package interfaces;

import java.util.Comparator;
import java.util.Date;

/**
 * Created by aleksey on 17.06.17.
 */
public class Employee implements Comparable<Employee> {

    /**
     *
     */
    private int id;
    /**
     *
     */
    private String name;
    /**
     *
     */
    private int salary;
    /**
     *
     */
    private int age;
    /**
     *
     */
    private Date dateOfJoining;

    /**
     *
     */
    public static final Comparator<Employee> AGE_COMPARATOR  = new Comparator<Employee>() {

        @Override
        public int compare(Employee oOne, Employee oTwo) {
            return oOne.age - oTwo.age;  // This will work because age is positive integer
        }

    };

    /**
     *
     */
    public static final Comparator<Employee> SALARY_COMPARATOR = new Comparator<Employee>() {

        @Override
        public int compare(Employee oOne, Employee oTwo) {
            return oOne.salary - oTwo.salary; // salary is also positive integer
        }

    };

    /**
     *
     */
    public static final Comparator<Employee> NAME_COMPARATOR = new Comparator<Employee>() {



        @Override
        public int compare(Employee oOne, Employee oTwo) {
            return oOne.name.compareTo(oTwo.name);
        }

    };

    /**
     *
     */
    public static final Comparator<Employee> DATE_OF_JOINING_COMPARATOR = new Comparator<Employee>() {

        @Override
        public int compare(Employee oOne, Employee oTwo) {
            return oOne.dateOfJoining.compareTo(oTwo.dateOfJoining);
        }

    };

    public Employee(int id, String name, int salary, int age, Date dateOfJoining) {
        this.id = id;
        this.name = name;
        this.salary = salary;
        this.age = age;
        this.dateOfJoining = dateOfJoining;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", salary=" + salary
                + ", age=" + age + ", dateOfJoining=" + dateOfJoining + '}';

    }

    @Override
    public int compareTo(Employee o) {
        return this.id - o.id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }

        final Employee other = (Employee) obj;

        if (this.id != other.id) {
            return false;
        }
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        if (this.salary != other.salary) {
            return false;
        }
        if (this.age != other.age) {
            return false;
        }
        if (this.dateOfJoining != other.dateOfJoining
                && (this.dateOfJoining == null || !this.dateOfJoining.equals(other.dateOfJoining))) {

            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + this.id;
        hash = 47 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 47 * hash + this.salary;
        hash = 47 * hash + this.age;
        hash = 47 * hash + (this.dateOfJoining != null ? this.dateOfJoining.hashCode() : 0);
        return hash;
    }

}
