package operators;

/**
 * Created by johnny on 26.06.17.
 */
public class TernaryOperator {
    /**
     *
     */
    private Integer first;
    /**
     *
     */
    private Integer second;

    public Integer getFirst() {
        return first;
    }

    public Integer getSecond() {
        return second;
    }

    public TernaryOperator() {
    }

    public TernaryOperator(Integer first, Integer second) {
        this.first = first;
        this.second = second;
    }

    /**
     *
     * @param i
     * @param j
     * @return
     */
    public static int getMinValue(int i, int j) {
        return (i < j) ? i : j;
    }

    /**
     *
     * @param i
     * @return
     */
    public static int getAbsoluteValue(int i) {
        return (i < 0) ? -i : i;
    }

    /**
     *
     * @param b
     * @return
     */
    public static boolean invertBoolean(boolean b) {
        return b ? false : true;
    }

    /**
     *
     * @param str
     */
    public static void  containsA(String str) {
        String data = str.contains("A") ? "Str contains A" : "Str doesn't contains A";
        System.out.println(data);
    }

    /**
     *
     * @param x
     * @param ternaryOperator
     */
    public static void ternaryMethod(Integer x, TernaryOperator ternaryOperator) {
        System.out.println((x.equals(ternaryOperator.getFirst()))
        ? "i = 5" : ((x.equals(ternaryOperator.getSecond())) ? "i = 10" : "i is not equal 5 or 10"));
    }

}
